package org.launchcode.training;


import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CarTest {
    static final double TOLERANCE = 0.00000001d;

    Car car;

    @Before
    public void createCar(){
        car = new Car("Honda", "CR-V", 13, 26);
    }

    //constructor sets gasTankLevel properly
    @Test
    public void ConstructorSetsGasTankLevel(){
        assertEquals(13d, car.getGasTankLevel(), TOLERANCE);
    }

    //gasTankLevel is accurate after driving within tank range
    @Test
    public void GasTankLevelIsAccurateAfterDriving(){
        car.drive(26);
        assertEquals(12d, car.getGasTankLevel(), TOLERANCE);
    }

    //gasTankLevel is accurate after attempting to drive past tank range
    @Test
    public void GasTankLevelIsAccurateAfterDrivingPastRange(){
        car.drive(4600);
        assertEquals(0, car.getGasTankLevel(), TOLERANCE);
    }
    
    //can't have more gas than tank size, expect an exception
    @Test(expected = IllegalArgumentException.class)
    public void CarCantFillPastCapacity(){
        car.setGasTankLevel(14);
    }

    @Test
    public void addGas(){
        car.drive(260);
        car.addGas(5);
        assertEquals(8, car.getGasTankLevel(), TOLERANCE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addGasThrowsExceptionWhenTooMuchGasIsAdded(){
        car.addGas(5);
    }
    
}
